const express = require("express");
var cors = require('cors');
const path = require("path");
var parser = require('rss-parser');

var app = express();
app.use(cors());

app.set("view engine", "ejs");

const port = process.env.PORT || 8000;

app.get("/", function(request, response) {
    var datas = [];
    parser.parseURL('https://keddr.com/feed/', function(err, parsed) {
        console.log(parsed.feed.title);
        parsed.feed.entries.forEach(function(entry) {
            var dataIt = {
                title: entry.title,
                link: entry.link,
                creator: entry.creator,
                content: entry.contentSnippet,
                id: "#it"
            }
            datas.push(dataIt);
        });

        parser.parseURL('https://veddro.com/?feed=yandexfeed', function(err, parsed) {
            console.log(parsed.feed.title);
            parsed.feed.entries.forEach(function(entry) {
                var dataCars = {
                    title: entry.title,
                    link: entry.link,
                    creator: entry.creator,
                    content: entry.contentSnippet,
                    id: "#cars"
                }
                datas.push(dataCars);
            });
            response.render("index", {
                title: "News",
                emailsVisible: true,
                data: datas,
                phone: "+3806678643"
            })

        })

    })
});

app.listen(port, () => {
    console.log(`app start on port ${port}`)
})